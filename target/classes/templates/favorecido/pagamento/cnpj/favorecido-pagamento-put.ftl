{
  "favorecidoPagamento": {
    "tipoDePessoa": "${favorecidoPagamento.tipoDePessoa}",
    "tipoDeParceiro": "${favorecidoPagamento.tipoDeParceiro}",
    "razaoSocial": "${favorecidoPagamento.razaoSocial}",
    "nomeFantasia": "${favorecidoPagamento.nomeFantasia}",
    "cnpj": "${favorecidoPagamento.cnpj}",
    <#if favorecidoPagamento.inscricaoMunicipal??>
      "inscricaoMunicipal": "${favorecidoPagamento.inscricaoMunicipal}",
    </#if>
    <#if favorecidoPagamento.inscricaoEstadual??>
      "inscricaoEstadual": "${favorecidoPagamento.inscricaoEstadual}",
    </#if>
    "cep": "${favorecidoPagamento.cep}",
    "logradouro": "${favorecidoPagamento.logradouro}",
    "numero": ${favorecidoPagamento.numero?c},
    <#if favorecidoPagamento.complemento??>
      "complemento": "${favorecidoPagamento.complemento}",
    </#if>
    "bairro": "${favorecidoPagamento.bairro}",
    "cidade": "${favorecidoPagamento.cidade}",
    "estado": "${favorecidoPagamento.estado}",
    "telefone": ${favorecidoPagamento.telefone?c},
    "tipoFavorecidos":[<#list favorecidoPagamento.tipoFavorecidos as tipoFavorecido>{
      "id": "${tipoFavorecido.id}",
      "codigo": ${tipoFavorecido.codigo},
      "favorecido": {
        "id": "${tipoFavorecido.favorecido.id}",
        "codigo": "${tipoFavorecido.favorecido.codigo}",
        "codigo-empresa": "${tipoFavorecido.favorecido.codigo\-empresa}",
        "codigo-tipo-de-dominio": ${tipoFavorecido.favorecido.codigo\-tipo\-de\-dominio},
        "descricao": "${tipoFavorecido.favorecido.descricao}",
        "status": ${tipoFavorecido.favorecido.status?c},
        "nome": "${tipoFavorecido.favorecido.nome}",
        "matricula": "${tipoFavorecido.favorecido.matricula}",
        "data": "${tipoFavorecido.favorecido.data}",
        "hora": "${tipoFavorecido.favorecido.hora}"
      },
      "tipoPagamentos": [<#list tipoFavorecido.tipoPagamentos as pagamento>{
  	    "id": "${pagamento.id}",
        "codigo": ${pagamento.codigo},
        "descricao": "${pagamento.descricao}",
        <#if pagamento.tributavel==true>
          "codigoRetencao": "${pagamento.codigoRetencao}",
          "codigoServicoTributavelSAP": {
            "id": "${pagamento.codigoServicoTributavelSAP.id}",
            "codigo": "${pagamento.codigoServicoTributavelSAP.codigo}",
            "codigo-empresa": "${pagamento.codigoServicoTributavelSAP.codigo\-empresa}",
            "codigo-tipo-de-dominio": ${pagamento.codigoServicoTributavelSAP.codigo\-tipo\-de\-dominio},
            "descricao": "${pagamento.codigoServicoTributavelSAP.descricao}",
            "status": ${pagamento.codigoServicoTributavelSAP.status?c},
            "nome": "${pagamento.codigoServicoTributavelSAP.nome}",
            "matricula": "${pagamento.codigoServicoTributavelSAP.matricula}",
            "data": "${pagamento.codigoServicoTributavelSAP.data}",
            "hora": "${pagamento.codigoServicoTributavelSAP.hora}"
          },
        </#if>
        "origemPagamentoSAP": {
          "id": "${pagamento.origemPagamentoSAP.id}",
          "codigo": "${pagamento.origemPagamentoSAP.codigo}",
          "codigo-empresa": "${pagamento.origemPagamentoSAP.codigo\-empresa}",
          "codigo-tipo-de-dominio": ${pagamento.origemPagamentoSAP.codigo\-tipo\-de\-dominio},
          "descricao": "${pagamento.origemPagamentoSAP.descricao}",
          "status": ${pagamento.origemPagamentoSAP.status?c},
          "nome": "${pagamento.origemPagamentoSAP.nome}",
          "matricula": "${pagamento.origemPagamentoSAP.matricula}",
          "data": "${pagamento.origemPagamentoSAP.data}",
          "hora": "${pagamento.origemPagamentoSAP.hora}"
        },
        "meioPagamentos": [<#list pagamento.meioPagamentos as item>{
          "id": "${item.id}",
          "codigo": "${item.codigo}",
          "codigo-empresa": "${item.codigo\-empresa}",
          "codigo-tipo-de-dominio": ${item.codigo\-tipo\-de\-dominio},
          "descricao": "${item.descricao}",
          "status": ${item.status?c},
          "nome": "${item.nome}",
          "matricula": "${item.matricula}",
          "data": "${item.data}",
          "hora": "${item.hora}"
        }<#sep>,</#sep></#list>],
        "faturamentoReverso": ${pagamento.faturamentoReverso?c},
        "tributavel": ${pagamento.tributavel?c},
        "tipoInterface": ${pagamento.tipoInterface?c},
        "dataCadastro": "${pagamento.dataCadastro}",
        "ativo": ${pagamento.ativo?c},
        "usuario": {
          "nome": "${pagamento.usuario.nome}",
          "matricula": "${pagamento.usuario.matricula}"
        }
      }<#sep>,</#sep></#list>],
      "versao": "${tipoFavorecido.versao}",
      "dataCadastro": "${tipoFavorecido.dataCadastro}",
      "ativo": ${tipoFavorecido.ativo?c},
      "usuario": {
        "nome": "${tipoFavorecido.usuario.nome}",
        "matricula": "${tipoFavorecido.usuario.matricula}"
      }
    }<#sep>,</#sep></#list>],
    "dadosBancariosDosTiposDeFavorecidos": [<#list favorecidoPagamento.dadosBancariosDosTiposDeFavorecidos as dadosBancariosDoTipoDeFavorecido>{
      "contaPrincipal": ${dadosBancariosDoTipoDeFavorecido.contaPrincipal?c},
      "codigoTipoDeFavorecido": "${dadosBancariosDoTipoDeFavorecido.codigoTipoDeFavorecido}",
      "descricaoTipoDeFavorecido": "${dadosBancariosDoTipoDeFavorecido.descricaoTipoDeFavorecido}",
      "tipoDeConta": "${dadosBancariosDoTipoDeFavorecido.tipoDeConta}",
      "banco": "${dadosBancariosDoTipoDeFavorecido.banco}",
      "agenciaNumero": "${dadosBancariosDoTipoDeFavorecido.agenciaNumero}",
      <#if dadosBancariosDoTipoDeFavorecido.agenciaDigito??>
        "agenciaDigito": "${dadosBancariosDoTipoDeFavorecido.agenciaDigito}",
      </#if>
      <#if dadosBancariosDoTipoDeFavorecido.email??>
        "email": "${dadosBancariosDoTipoDeFavorecido.email}",
      </#if>
      <#if dadosBancariosDoTipoDeFavorecido.observacao??>
        "observacao": "${dadosBancariosDoTipoDeFavorecido.observacao}",
      </#if>
      "contaNumero": "${dadosBancariosDoTipoDeFavorecido.contaNumero}",
      "contaDigito": "${dadosBancariosDoTipoDeFavorecido.contaDigito}"
    }<#sep>,</#sep></#list>],
   	"codigo": "${favorecidoPagamento.codigo?c}",
  	"versao":"${favorecidoPagamento.versao?c}",
    "dataCadastro": "${favorecidoPagamento.dataCadastro}",
    "ativo": ${favorecidoPagamento.ativo?c},
    "usuario": {
      "nome": "${favorecidoPagamento.usuario.nome}",
      "matricula": "${favorecidoPagamento.usuario.matricula}"
    }
  }
}